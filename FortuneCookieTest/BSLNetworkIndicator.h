//
//  BSLNetworkIndicator.h
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 9/10/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSLNetworkIndicator : NSObject
{
    int _counter;
}

+ (BSLNetworkIndicator*) shared;

- (void) startDownloading;
- (void) finishedDownloading;

@end
