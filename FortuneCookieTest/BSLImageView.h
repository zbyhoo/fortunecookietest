//
//  BSLImageView.h
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 9/10/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BSLFlickrPhoto;
@class BSLDetailViewController;

@interface BSLImageView : UIImageView
{
    NSMutableData* _imageData;
    BOOL _downloadStarted;
    BOOL _downloadFinished;
}

@property (nonatomic, assign) BSLFlickrPhoto* flickrPhoto;
@property (nonatomic, assign) BSLDetailViewController* detailViewController;

- (void) loadPhoto;

@end
