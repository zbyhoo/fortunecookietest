//
//  BSLFlickrPhoto.m
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 9/10/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import "BSLFlickrPhoto.h"

static NSString* const AUTHOR_TAG = @"author";
static NSString* const DESCRIPTION_TAG = @"media:description";
static NSString* const DATE_TAG = @"dc:date.Taken";

@implementation BSLFlickrPhoto

- (id) init
{
    self = [super init];
    if (self)
    {
        self.values = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void) dealloc
{
    self.author = nil;
    self.photoSmallURL = nil;
    self.photoBigURL = nil;
    self.description = nil;
    self.date = nil;
    self.values = nil;
    self.photoBig = nil;
    self.photoSmall = nil;
    
    [super dealloc];
}

- (void) parseValues
{
    self.author = [self.values objectForKey:AUTHOR_TAG];
    self.author = [self.author substringFromIndex:[self.author rangeOfString:@"("].location + 1];
    self.author = [self.author substringToIndex:self.author.length - 1];
    
    self.description = [self.values objectForKey:DESCRIPTION_TAG];
    self.date = [self.values objectForKey:DATE_TAG];
    self.date = [self.date substringToIndex:10];
}

@end
