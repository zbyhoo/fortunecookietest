//
//  main.m
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 8/30/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BSLAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BSLAppDelegate class]));
    }
}
