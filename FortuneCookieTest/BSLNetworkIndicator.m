//
//  BSLNetworkIndicator.m
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 9/10/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import "BSLNetworkIndicator.h"

@implementation BSLNetworkIndicator

+ (BSLNetworkIndicator*) shared
{
    static BSLNetworkIndicator* indicator = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        indicator = [[self alloc] init];
    });
    return indicator;
}

- (void) startDownloading
{
    if (_counter++ == 0)
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:YES];
    }
}
- (void) finishedDownloading
{
    if (--_counter == 0)
    {
        [[UIApplication sharedApplication] setNetworkActivityIndicatorVisible:NO];
    }
}


@end
