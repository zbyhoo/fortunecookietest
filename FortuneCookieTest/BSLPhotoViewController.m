//
//  BSLPhotoViewController.m
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 9/10/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import "BSLPhotoViewController.h"
#import "BSLFlickrPhoto.h"
#import "BSLNetworkIndicator.h"

@interface BSLPhotoViewController ()

@end

@implementation BSLPhotoViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = NSLocalizedString(@"DETAILS", @"DETAILS");

    
    if (self.flickrPhoto.photoBig)
    {
        self.imageView.image = self.flickrPhoto.photoBig;
    }
    else
    {
        if (self.flickrPhoto.photoSmall)
        {
            self.thumbnailView.image = self.flickrPhoto.photoSmall;
        }
        [self.activitiIndicatorView startAnimating];
        [[BSLNetworkIndicator shared] startDownloading];
        
        dispatch_queue_t queue = dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0ul);
        dispatch_async(queue, ^{
            NSData* data = [NSData dataWithContentsOfURL:self.flickrPhoto.photoBigURL];
            UIImage* image = [UIImage imageWithData:data];
            dispatch_sync(dispatch_get_main_queue(), ^{
                self.flickrPhoto.photoBig = image;
                self.imageView.image = image;
                [self.activitiIndicatorView stopAnimating];
                [[BSLNetworkIndicator shared] finishedDownloading];
                
                self.imageView.alpha = 0.0f;
                [UIView beginAnimations:@"fade in" context:nil];
                [UIView setAnimationDuration:0.5f];
                self.imageView.alpha = 1.0f;
                self.thumbnailView.alpha = 0.0f;
                [UIView commitAnimations];
            });
        });
    }
    
    
    self.authorLabel.text = self.flickrPhoto.author;
    self.dateLabel.text = self.flickrPhoto.date;
    [self.webView loadHTMLString:self.flickrPhoto.description baseURL:nil];
    self.webView.scrollView.bounces = NO;
    [self.webView.scrollView flashScrollIndicators];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

- (BOOL)                webView:(UIWebView*)webView
     shouldStartLoadWithRequest:(NSURLRequest*)request
                 navigationType:(UIWebViewNavigationType)navigationType
{
    if (!_initialDescriptionLoad)
    {
        _initialDescriptionLoad = YES;
        return YES;
    }
    else
    {
        return NO;
    }
}

@end
