//
//  BSLAppDelegate.h
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 8/30/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BSLAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) UINavigationController *navigationController;

@end
