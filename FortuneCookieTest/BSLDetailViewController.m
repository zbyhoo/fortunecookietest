//
//  BSLDetailViewController.m
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 8/30/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import "BSLDetailViewController.h"
#import "BSLRSSParser.h"
#import "BSLFlickrPhoto.h"
#import "BSLImageView.h"
#import "BSLPhotoViewController.h"
#import "BSLNetworkIndicator.h"

@interface BSLDetailViewController ()
- (void)configureView;
- (void) downloadFeed;
- (void) parseFeed;
- (void) displayResults;
@end

@implementation BSLDetailViewController

- (void)dealloc
{
    [_detailItem release];
    [super dealloc];
}

#pragma mark - Managing the detail item

- (void)setDetailItem:(id)newDetailItem
{
    if (_detailItem != newDetailItem) {
        [_detailItem release];
        _detailItem = [newDetailItem retain];

        // Update the view.
        [self configureView];
    }
}

- (void)configureView
{
    // Update the user interface for the detail item.
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    [self configureView];
    [self downloadFeed];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation != UIInterfaceOrientationPortraitUpsideDown);
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = NSLocalizedString(@"GALLERY", @"GALLERY");
    }
    return self;
}
				
- (void) downloadFeed
{
    NSURL* url = [NSURL URLWithString:[NSString stringWithFormat:
                                       @"http://api.flickr.com/services/feeds/photos_public.gne?format=rss2&tags=%@",
                                       [self.detailItem description]]];
    NSURLRequest* request = [NSURLRequest requestWithURL:url];
    [[[NSURLConnection alloc] initWithRequest:request delegate:self] autorelease];
    
    [[BSLNetworkIndicator shared] startDownloading];
    
    [self.activityIndicatorView startAnimating];
    self.title = NSLocalizedString(@"Loading", @"Loading");
    
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (_feedData)
    {
        [_feedData release];
    }
    _feedData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_feedData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSString* title = NSLocalizedString(@"Connection Error", @"Connection Error");
    NSString* cancelButton = NSLocalizedString(@"OK", @"OK");
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:cancelButton
                                              otherButtonTitles:nil];
    [alertView show];
    [alertView autorelease];
    
    [self.activityIndicatorView stopAnimating];
    self.title = NSLocalizedString(@"GALLERY (offline)", @"GALLERY (offline)");
    
    [_feedData release];
    _feedData = nil;
    
    [[BSLNetworkIndicator shared] finishedDownloading];
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    [self parseFeed];
    
    [_feedData release];
    _feedData = nil;
    
    [[BSLNetworkIndicator shared] finishedDownloading];
}

- (void) parseFeed
{
    NSXMLParser* xmlParser = [[NSXMLParser alloc] initWithData:_feedData];
    if (_rssParser)
    {
        [_rssParser release];
        _rssParser = nil;
    }
    _rssParser = [[BSLRSSParser alloc] init];
    xmlParser.delegate = _rssParser;
    BOOL success = [xmlParser parse];
    
    if (success)
    {
        if (_rssParser.photos.count > 0)
        {
            [self displayResults];
        }
        else
        {
            [self.activityIndicatorView stopAnimating];
            self.title = NSLocalizedString(@"GALLERY", @"GALLERY");
        }
    }
    else
    {
        NSString* title = NSLocalizedString(@"Parse Error", @"Parse Error");
        NSString* description = NSLocalizedString(@"Unable to parse rss feed", @"Unable to parse rss feed");
        NSString* cancelButton = NSLocalizedString(@"OK", @"OK");
        
        UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title
                                                            message:description
                                                           delegate:nil
                                                  cancelButtonTitle:cancelButton
                                                  otherButtonTitles:nil];
        [alertView show];
        [alertView autorelease];
        
        [self.activityIndicatorView stopAnimating];
        self.title = NSLocalizedString(@"GALLERY (error)", @"GALLERY (error)");
    }
}

- (void) displayResults
{
    const int xStep = (self.view.frame.size.width - 5) / 4.0f;
    const int yStep = xStep;
    
    int photoCount = 0;
    int x = 1;
    int y = 1;
    
    for (BSLFlickrPhoto* photo in _rssParser.photos)
    {
        BSLImageView* imageView = [[BSLImageView alloc] initWithFrame:CGRectMake(x, y, xStep, yStep)];
        imageView.flickrPhoto = photo;
        imageView.detailViewController = self;
        [imageView loadPhoto];
        
        [self.scrollView addSubview:imageView];
        [imageView autorelease];
        
        ++photoCount;
        
        if (photoCount % 4 == 0)
        {
            x = 1;
            y += yStep + 1;
        }
        else
        {
            x += xStep + 1;
        }
    }
    
    [self startTimer];
    
    self.scrollView.contentSize = CGSizeMake(self.scrollView.contentSize.width,
                                             yStep * ceilf(photoCount / 4.0f));
}

- (void) showPhoto:(BSLFlickrPhoto*)photo
{
    BSLPhotoViewController* photoViewController = [[[BSLPhotoViewController alloc] initWithNibName:@"BSLPhotoViewController" bundle:nil] autorelease];
    photoViewController.flickrPhoto = photo;
    UIBarButtonItem *backButton = [[UIBarButtonItem alloc]
                                   initWithTitle: @"Back"
                                   style: UIBarButtonItemStyleBordered
                                   target: nil action: nil];
    
    [self.navigationItem setBackBarButtonItem: backButton];
    [backButton release];
    [self.navigationController pushViewController:photoViewController animated:YES];
}

- (void) checkFinishedDownloadingPhotos
{
    for (BSLFlickrPhoto* photo in _rssParser.photos)
    {
        if (!photo.photoSmall)
        {
            return;
        }
    }
    [self.activityIndicatorView stopAnimating];
    self.title = NSLocalizedString(@"GALLERY", @"GALLERY");
    [_timer invalidate];
}

- (void) startTimer
{
    _timer = [NSTimer scheduledTimerWithTimeInterval:0.5f target:self selector:@selector(checkFinishedDownloadingPhotos) userInfo:nil repeats:YES];
}

@end
