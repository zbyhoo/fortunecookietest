//
//  BSLPhotoViewController.h
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 9/10/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BSLFlickrPhoto;

@interface BSLPhotoViewController : UIViewController <UIWebViewDelegate>
{
    BOOL _initialDescriptionLoad;
}

@property (nonatomic, assign) BSLFlickrPhoto* flickrPhoto;

@property (nonatomic, strong) IBOutlet UIImageView* imageView;
@property (nonatomic, strong) IBOutlet UIImageView* thumbnailView;
@property (nonatomic, strong) IBOutlet UILabel* authorLabel;
@property (nonatomic, strong) IBOutlet UILabel* dateLabel;
@property (nonatomic, strong) IBOutlet UIWebView* webView;
@property (nonatomic, strong) IBOutlet UIActivityIndicatorView* activitiIndicatorView;

@end
