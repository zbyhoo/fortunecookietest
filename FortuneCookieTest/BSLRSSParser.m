//
//  BSLRSSParser.m
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 9/10/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import "BSLRSSParser.h"
#import "BSLFlickrPhoto.h"

static NSString* const ITEM_TAG = @"item";
static NSString* const MEDIA_CONTENT_TAG = @"media:content";
static NSString* const MEDIA_THUMBNAIL_TAG = @"media:thumbnail";
static NSString* const URL_ATTRIB = @"url";

@implementation BSLRSSParser

- (id) init
{
    self = [super init];
    if (self)
    {
        self.photos = [NSMutableArray array];
    }
    return self;
}

- (void) dealloc
{
    self.photos = nil;
    [_currentPhoto release];
    _currentPhoto = nil;
    
    [_elementName release];
    _elementName = nil;
    
    [_elementValue release];
    _elementValue = nil;
    
    [super dealloc];
}

-(void)         parser:(NSXMLParser *)parser
       didStartElement:(NSString *)elementName
          namespaceURI:(NSString *)namespaceURI
         qualifiedName:(NSString *)qualifiedName
            attributes:(NSDictionary *)attributeDict
{
    if([elementName isEqualToString:ITEM_TAG])
    {
        _currentPhoto = [[BSLFlickrPhoto alloc] init];
    }
    else if ([elementName isEqualToString:MEDIA_CONTENT_TAG])
    {
        NSString* imageUrl = [attributeDict objectForKey:URL_ATTRIB];
        _currentPhoto.photoBigURL = [NSURL URLWithString:imageUrl];
    }
    else if ([elementName isEqualToString:MEDIA_THUMBNAIL_TAG])
    {
        NSString* imageUrl = [attributeDict objectForKey:URL_ATTRIB];
        _currentPhoto.photoSmallURL = [NSURL URLWithString:imageUrl];
    }
    else
    {
        if (_currentPhoto)
        {
            _elementName = [elementName copy];
            
            [_elementValue release];
            _elementValue = [[NSMutableString alloc] init];
        }
    }
}

- (void)        parser:(NSXMLParser *)parser
         didEndElement:(NSString *)elementName
          namespaceURI:(NSString *)namespaceURI
         qualifiedName:(NSString *)qName
{
    if([elementName isEqualToString:ITEM_TAG])
    {
        [self.photos addObject:_currentPhoto];
        [_currentPhoto parseValues];
        [_currentPhoto release];
        _currentPhoto = nil;
    }
    else
    {
        if (_currentPhoto)
        {
            if (_elementName && _elementValue)
            {
                [_currentPhoto.values setObject:_elementValue forKey:_elementName];
                [_elementName release];
                _elementName = nil;
                [_elementValue release];
                _elementValue = nil;
            }
        }
    }
}

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if (_elementName && _currentPhoto)
    {
        [_elementValue appendString:string];
    }
}

@end
