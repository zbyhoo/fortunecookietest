//
//  BSLDetailViewController.h
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 8/30/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BSLRSSParser;
@class BSLFlickrPhoto;

@interface BSLDetailViewController : UIViewController
{
    NSMutableData* _feedData;
    BSLRSSParser* _rssParser;
    NSTimer* _timer;
}

@property (strong, nonatomic) id detailItem;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView* activityIndicatorView;
@property (strong, nonatomic) IBOutlet UIScrollView* scrollView;

- (void) showPhoto:(BSLFlickrPhoto*)photo;

@end
