//
//  BSLFlickrPhoto.h
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 9/10/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BSLFlickrPhoto : NSObject

@property (strong, nonatomic) NSString* author;
@property (strong, nonatomic) NSURL* photoSmallURL;
@property (strong, nonatomic) NSURL* photoBigURL;
@property (strong, nonatomic) NSString* description;
@property (strong, nonatomic) NSString* date;
@property (strong, nonatomic) NSMutableDictionary* values;

@property (strong, nonatomic) UIImage* photoBig;
@property (strong, nonatomic) UIImage* photoSmall;

- (void) parseValues;

@end
