//
//  BSLImageView.m
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 9/10/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import "BSLImageView.h"
#import "BSLFlickrPhoto.h"
#import "BSLDetailViewController.h"
#import "BSLNetworkIndicator.h"

@implementation BSLImageView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.userInteractionEnabled = YES;
        self.contentMode = UIViewContentModeScaleAspectFit;
    }
    return self;
}

- (void) dealloc
{
    if (_downloadStarted && !_downloadFinished)
    {
        [[BSLNetworkIndicator shared] finishedDownloading];
    }
    
    [super dealloc];
}

- (BOOL) canBecomeFirstResponder
{
    return YES;
}

- (void) touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.detailViewController showPhoto:self.flickrPhoto];
}

- (void) loadPhoto
{
    [[BSLNetworkIndicator shared] startDownloading];
    _downloadStarted = YES;
    NSURLRequest* request = [NSURLRequest requestWithURL:self.flickrPhoto.photoSmallURL];
    [[[NSURLConnection alloc] initWithRequest:request delegate:self] autorelease];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if (_imageData)
    {
        [_imageData release];
    }
    _imageData = [[NSMutableData alloc] init];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    [_imageData appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    NSString* title = NSLocalizedString(@"Connection Error", @"Connection Error");
    NSString* cancelButton = NSLocalizedString(@"OK", @"OK");
    
    UIAlertView* alertView = [[UIAlertView alloc] initWithTitle:title
                                                        message:error.localizedDescription
                                                       delegate:nil
                                              cancelButtonTitle:cancelButton
                                              otherButtonTitles:nil];
    [alertView show];
    [alertView autorelease];
    
    [_imageData release];
    _imageData = nil;
    
    [[BSLNetworkIndicator shared] finishedDownloading];
    _downloadFinished = YES;
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    UIImage* image = [UIImage imageWithData:_imageData];
    [_imageData release];
    _imageData = nil;
    
    self.image = image;
    self.flickrPhoto.photoSmall = image;
    self.alpha = 0.0f;
    [UIView beginAnimations:@"fade in" context:nil];
    [UIView setAnimationDuration:0.5f];
    self.alpha = 1.0f;
    [UIView commitAnimations];
    [[BSLNetworkIndicator shared] finishedDownloading];
    _downloadFinished = YES;
}


@end
