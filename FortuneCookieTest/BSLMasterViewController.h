//
//  BSLMasterViewController.h
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 8/30/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import <UIKit/UIKit.h>

@class BSLDetailViewController;

@interface BSLMasterViewController : UITableViewController <UISearchBarDelegate>

@property (strong, nonatomic) IBOutlet UISearchBar *searchBar;

@end
