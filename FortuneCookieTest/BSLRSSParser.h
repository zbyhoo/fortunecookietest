//
//  BSLRSSParser.h
//  FortuneCookieTest
//
//  Created by Zbigniew Kominek on 9/10/12.
//  Copyright (c) 2012 Zbigniew Kominek. All rights reserved.
//

#import <Foundation/Foundation.h>

@class BSLFlickrPhoto;

@interface BSLRSSParser : NSObject <NSXMLParserDelegate>
{
    NSMutableString* _elementValue;
    NSString* _elementName;
    BSLFlickrPhoto* _currentPhoto;
}

@property (strong, nonatomic) NSMutableArray* photos;

@end
